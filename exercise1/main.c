/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/14/2020
 */

#include <stdio.h>
#define MAX_MATRIX_ROWS 50
#define MAX_MATRIX_COLUMNS 50
#define WHITE 0
#define BLACK 1

typedef struct {
    int x;
    int y;
    int width;
    int height;
    int maxValue;
} cell;

void printCell(cell value, char type[]);

int main() {
    char filename[31];
    FILE * fi;
    int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS];
    int rows, columns, white;
    cell maxWidth = {-1, -1, 0, 0, 0};
    cell maxHeight = {-1, -1, 0, 0, 0};
    cell maxSquare = {-1, -1, 0, 0, 0};
    cell tmpCell;

    printf("Insert the input file name: ");
    scanf("%s", filename);

    fi = fopen(filename, "r");

    if (fi == NULL){
        printf("Invalid file.");
        return -1;
    }

    fscanf(fi, "%d%d", &rows, &columns);

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            fscanf(fi, "%d", &matrix[i][j]);
        }
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            if (matrix[i][j] == BLACK) {
                tmpCell.x = i;
                tmpCell.y = j;
                white = 0;

                for (int k = i; (k < rows && !white); k++) {
                    if (matrix[k][j] == WHITE || k == rows - 1) {
                        tmpCell.height = k - i + ( (k == rows - 1) ? 1 : 0);

                        white = 1;
                    }
                }

                white = 0;

                for (int l = j; (l < columns && !white); l++) {
                    if (matrix[i][l] == WHITE || l == columns - 1) {
                        tmpCell.width = l - j + ( (l == columns - 1) ? 1 : 0);

                        white = 1;
                    }
                }

                if (tmpCell.width > maxWidth.maxValue) {
                    tmpCell.maxValue = tmpCell.width;
                    maxWidth = tmpCell;
                }

                if (tmpCell.height > maxHeight.maxValue) {
                    tmpCell.maxValue = tmpCell.height;
                    maxHeight = tmpCell;
                }

                if ((tmpCell.width * tmpCell.height) > maxSquare.maxValue) {
                    tmpCell.maxValue = tmpCell.width * tmpCell.height;
                    maxSquare = tmpCell;
                }
            }
        }
    }

    printCell(maxWidth, "Base");
    printCell(maxHeight, "Altezza");
    printCell(maxSquare, "Area");

    return 0;
}

void printCell(cell value, char type[])
{
    printf("Max %s: estr. sup. SX=<%d, %d>, b=%d, h=%d, Area=%d\n", type, value.x, value.y, value.width, value.height, value.width * value.height);
}
