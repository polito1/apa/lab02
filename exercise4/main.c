/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/14/2020
 */

#include <stdio.h>

#define MAX_ARRAY_LENGTH 50

void printArray(int array[MAX_ARRAY_LENGTH], int length);
void printInformations(int externalLoops, int swaps, int totalLoops, const int array[MAX_ARRAY_LENGTH], int length);
void arrayCopy(const int src[MAX_ARRAY_LENGTH], int destination[MAX_ARRAY_LENGTH], int length);
void selectionSort(const int array[MAX_ARRAY_LENGTH], int length);
void insertionSort(const int array[MAX_ARRAY_LENGTH], int length);
void shellSort(const int array[MAX_ARRAY_LENGTH], int length);

int main() {
    int array[MAX_ARRAY_LENGTH];
    int length;
    int rows;
    int counter = 0;

    // Apro il file.
    FILE *fp = fopen("sort.txt", "r");

    // Gestione dei problemi legati al file.
    if (fp == NULL) {
        printf("Invalid file.");
        return -1;
    }

    // Leggo il numero di sequenze.
    fscanf(fp, "%d", &rows);

    while(counter <= rows && !feof(fp)) {
        // Leggo il numero di valori nella sequenza.
        fscanf(fp, "%d", &length);

        // Leggo la sequenza nell'array.
        for (int i = 0; i < length; i++) {
            fscanf(fp, "%d", &array[i]);
        }

        // Stampo la sequenza
        printf("\nSequence: ");
        printArray(array, length);

        //Eseguo i 3 algoritmi.
        selectionSort(array, length);
        insertionSort(array, length);
        shellSort(array, length);
    }

    return 0;
}

void selectionSort(const int array[MAX_ARRAY_LENGTH], int length)
{
    int copy[MAX_ARRAY_LENGTH];
    int tmp;
    int minIndex;

    int swaps = 0, externalLoops = 0, internalLoops = 0, totalLoops = 0;

    printf("Selection sort: \n");

    // Poichè dobbiamo analizzare i dati da più algoritmi, per non alterare l'array di partenza ne creiamo
    // una copia locale nelle singole implementazioni.
    arrayCopy(array, copy, length);

    for (int count = 0; count < length - 1; count ++) {
        // Incremento gli indicatori e resetto l'indicatore per i cicli interni.
        externalLoops++;
        internalLoops = 0;
        totalLoops++;
        minIndex = count;
        for (int j = count + 1; j < length; j++) {
            // Incremento l'indicatore dei cicli interni.
            internalLoops++;
            if (copy[j] < copy[minIndex]) {
                minIndex = j;
            }
        }
        printf("\tExternal loop: %d, Internal loops: %d\n", externalLoops, internalLoops);
        totalLoops += internalLoops;

        // Swap dei valori.
        // Se minIndex == count, significa che non ci sono valori più bassi, posso quindi evitare lo swap.
        if (minIndex != count) {
            swaps++;
            tmp = copy[count];
            copy[count] = copy[minIndex];
            copy[minIndex] = tmp;
        }
    }
    // Stampo le informazioni.
    printInformations(externalLoops, swaps, totalLoops, copy, length);
}

void insertionSort(const int array[MAX_ARRAY_LENGTH], int length)
{
    int copy[MAX_ARRAY_LENGTH];

    int tmp, j, swaps = 0, externalLoops = 0, internalLoops = 0, totalLoops = 0;

    // Poichè dobbiamo analizzare i dati da più algoritmi, per non alterare l'array di partenza ne creiamo
    // una copia locale nelle singole implementazioni.
    arrayCopy(array, copy, length);

    printf("Insertion sort: \n");
    for (int count = 1; count < length; count++) {
        externalLoops++;
        totalLoops++;
        tmp = copy[count];
        j = count - 1;

        internalLoops = 0;
        while (j >= 0 && tmp < copy[j]) {
            internalLoops++;
            swaps++;
            copy[j + 1] = copy[j];
            j--;
        }
        printf("\tExternal loop: %d, Internal loops: %d\n", externalLoops, internalLoops);
        totalLoops += internalLoops;

        copy[j + 1] = tmp;
    }
    printInformations(externalLoops, swaps, totalLoops, copy, length);
}

// Implementazione dell'algoritmo Shell Sort.
void shellSort(const int array[MAX_ARRAY_LENGTH], int length)
{
    int h = 1, swaps = 0, externalLoops = 0, totalLoops = 0, internalLoops, tmp, j;
    int copy[MAX_ARRAY_LENGTH];

    arrayCopy(array, copy, length);

    printf("Shell sort: \n");

    while (h < length/3) {
        h = (3 * h) + 1;
    }

    while (h >= 1) {
        for (int i = h; i < length; i++) {
            externalLoops++;
            totalLoops++;
            internalLoops = 0;
            tmp = copy[i];
            j = i;

            while (j >= h && tmp < copy[j-h]) {
                internalLoops++;
                swaps++;
                copy[j] = copy[j - h];
                j = j - h;
            }
            printf("\tExternal loop: %d, Internal loops: %d\n", externalLoops, internalLoops);
            totalLoops += internalLoops;

            copy[j] = tmp;
        }
        h = h / 3;
    }
    printInformations(externalLoops, swaps, totalLoops, copy, length);
}

// Stampa le prime `length` celle dell'array.
void printArray(int array[MAX_ARRAY_LENGTH], int length)
{
    for (int i = 0; i < length; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

// Copia `length` celle del vettore `src` nel vettore `destination`
void arrayCopy(const int src[MAX_ARRAY_LENGTH], int destination[MAX_ARRAY_LENGTH], int length)
{
    for (int i = 0; i < length; i++) {
        destination[i] = src[i];
    }
}

// Stampa i dati formattati
void printInformations(int externalLoops, int swaps, int totalLoops, const int array[MAX_ARRAY_LENGTH], int length)
{
    printf("\tExternal loops: %d\n", externalLoops);
    printf("\tSwaps: %d\n", swaps);
    printf("\tTotal loops: %d\n", totalLoops);
    printf("===\nVettore ordinato: ");
    printArray(array, length);
    printf("===\n");
}
