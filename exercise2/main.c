/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/14/2020
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_COMMAND_LENGTH 13
#define MAX_STRING_LENGTH 30
#define MAX_CORSE_LENGTH 1000

// Definisco un tipo enum per i comandi.
typedef enum {
    r_date,
    r_partenza,
    r_capolinea,
    r_ritardo,
    r_ritardo_tot,
    r_fine
} comando_e;

// Definisco una struct contenente le informazioni sulla corsa.
typedef struct {
    char codice_tratta[MAX_STRING_LENGTH + 1];
    char partenza[MAX_STRING_LENGTH + 1];
    char destinazione[MAX_STRING_LENGTH + 1];
    char data[MAX_STRING_LENGTH + 1];
    char ora_partenza[MAX_STRING_LENGTH + 1];
    char ora_arrivo[MAX_STRING_LENGTH + 1];
    int ritardo;
} corsa;

// Definisco una struct per poter lavorare in maniera più agevole con le date.
typedef struct {
    int day;
    int month;
    int year;
} data;

// Definizione delle funzioni che implementerò
comando_e leggiComando();
void date(corsa corse[MAX_CORSE_LENGTH], char inizio[MAX_STRING_LENGTH + 1], char fine[MAX_STRING_LENGTH + 1], int rows);
int between(data inizio, data fine, data value);
int dateCompare(data a, data b);
void printCorsa(corsa value);
void daFermata(corsa corse[MAX_CORSE_LENGTH], char fermata[MAX_STRING_LENGTH + 1], int rows);
void aFermata(corsa corse[MAX_CORSE_LENGTH], char fermata[MAX_STRING_LENGTH + 1], int rows);
void dateRitardi(corsa corse[MAX_CORSE_LENGTH], char inizio[MAX_STRING_LENGTH + 1], char fine[MAX_STRING_LENGTH + 1], int rows);
void ritardoComplessivo(corsa corse[MAX_CORSE_LENGTH], char codiceTratta[MAX_STRING_LENGTH + 1], int rows);
void strToLower(char str [MAX_STRING_LENGTH + 1]);

int main() {
    FILE * fp;
    int rows, counter = 0;
    fp = fopen("corse.txt", "r");
    corsa corse [MAX_CORSE_LENGTH];
    char dataInizio[MAX_STRING_LENGTH + 1], dataFine[MAX_STRING_LENGTH + 1];
    char fermata[MAX_STRING_LENGTH + 1], tratta[MAX_STRING_LENGTH + 1];
    comando_e comando;
    int continua = 1;

    if (fp == NULL) {
        printf("Invalid file.");
        return -1;
    }

    fscanf(fp, "%d", &rows);

    // Per ogni riga, scansiono una corsa.
    while (!feof(fp) && counter <= rows) {
        fscanf(
                fp,
                "%s %s %s %s %s %s %d",
                corse[counter].codice_tratta,
                corse[counter].partenza,
                corse[counter].destinazione,
                corse[counter].data,
                corse[counter].ora_partenza,
                corse[counter].ora_arrivo,
                &corse[counter].ritardo
        );

        counter ++;
    }

    // Chiudo il file: non è più utile.
    fclose(fp);

    while(continua) {
        // Leggo il comando.
        comando = leggiComando();

        // Eseguo un'azione diversa a seconda del caso.
        switch (comando) {
            case r_date:
                if (scanf("%s%s", dataInizio, dataFine) != 2) {
                    printf("Invalid arguments\n");
                    continua = 0;
                    break;
                }
                date(corse, dataInizio, dataFine, rows);
                break;
            case r_partenza:
                if (scanf("%s", fermata) != 1) {
                    printf("Invalid arguments\n");
                    continua = 0;
                    break;
                }
                strToLower(fermata);
                daFermata(corse, fermata, rows);
                break;
            case r_capolinea:
                if (scanf("%s", fermata) != 1) {
                    printf("Invalid arguments\n");
                    continua = 0;
                    break;
                }
                strToLower(fermata);
                aFermata(corse, fermata, rows);
                break;
            case r_ritardo:
                if (scanf("%s%s", dataInizio, dataFine) != 2) {
                    printf("Invalid arguments\n");
                    continua = 0;
                    break;
                }
                dateRitardi(corse, dataInizio, dataFine, rows);
                break;
            case r_ritardo_tot:
                if (scanf("%s", tratta) != 1) {
                    printf("Invalid arguments\n");
                    continua = 0;
                    break;
                }
                strToLower(tratta);
                ritardoComplessivo(corse, tratta, rows);
                break;
            case r_fine:
                continua = 0;
                break;
            default:
                printf("Invalid command.\n");
                continua = 0;
                break;
        }
    }
    return 0;
}

/*
 * Questa funzione legge il comando passato come input; grazie al tipo enum, possiamo utilizzare lo switch.
 */
comando_e leggiComando()
{
    comando_e c;
    char command[MAX_COMMAND_LENGTH + 1];
    char tabella[r_fine][MAX_COMMAND_LENGTH + 1] = {
            "date", "partenza", "capolinea", "ritardo", "ritardo_tot", "fine"
    };

    printf("Comando (date, partenza, capolinea, ritardo, ritardo_tot, fine): ");
    scanf("%s", command);

    strToLower(command);

    c = r_date;

    while (c < r_fine && strcmp(command, tabella[c]) != 0) {
        c++;
    }

    return c;
}

void date(corsa corse[MAX_CORSE_LENGTH], char inizio[MAX_STRING_LENGTH + 1], char fine[MAX_STRING_LENGTH + 1], int rows)
{
    data inizioData, fineData, tmpData;

    // In un thread sul canale dedicato su Slack, l'ingegner Pasini ha consigliato di utilizzare il formato
    // `anno-mese-giorno` per le date.
    sscanf(inizio, "%d-%d-%d", &inizioData.year, &inizioData.month, &inizioData.day);
    sscanf(fine, "%d-%d-%d", &fineData.year, &fineData.month, &fineData.day);

    for (int i = 0; i < rows; i++) {
        // Trasformo la stringa in una struct data.
        sscanf(corse[i].data, "%d-%d-%d", &tmpData.year, &tmpData.month, &tmpData.day);
        // Se la data della corsa è compresa tra le date passate come input, stampo la corsa.
        if (between(inizioData, fineData, tmpData)) {
            printCorsa(corse[i]);
        }
    }
}

void daFermata(corsa corse[MAX_CORSE_LENGTH], char fermata[MAX_STRING_LENGTH + 1], int rows)
{
    for (int i = 0; i < rows; i++) {
        // Normalizzo la partenza
        strToLower(corse[i].partenza);

        // Se la partenza corrisponde alla fermata passata, stampo la corsa.
        if (strcmp(corse[i].partenza, fermata) == 0) {
            printCorsa(corse[i]);
        }
    }
}

void aFermata(corsa corse[MAX_CORSE_LENGTH], char fermata[MAX_STRING_LENGTH + 1], int rows)
{
    for (int i = 0; i < rows; i++) {
        // Normalizzo la destinazione
        strToLower(corse[i].destinazione);

        // Se la destinazione corrisponde alla fermata passata, stampo la corsa.
        if (strcmp(corse[i].destinazione, fermata) == 0) {
            printCorsa(corse[i]);
        }
    }
}

void dateRitardi(corsa corse[MAX_CORSE_LENGTH], char inizio[MAX_STRING_LENGTH], char fine[MAX_STRING_LENGTH], int rows)
{
    data inizioData, fineData, tmpData;

    // In un thread sul canale dedicato su Slack, l'ingegner Pasini ha consigliato di utilizzare il formato
    // `anno-mese-giorno` per le date.
    sscanf(inizio, "%d-%d-%d", &inizioData.year, &inizioData.month, &inizioData.day);
    sscanf(fine, "%d-%d-%d", &fineData.year, &fineData.month, &fineData.day);

    for (int i = 0; i < rows; i++) {
        if (corse[i].ritardo > 0) {
            // Trasformo la stringa in una struct data in modo da poterla confrontare.
            sscanf(corse[i].data, "%d-%d-%d", &tmpData.year, &tmpData.month, &tmpData.day);

            // Se la corsa è avvenuta tra le due date passate come input, stampo la corsa.
            if (between(inizioData, fineData, tmpData)) {
                printCorsa(corse[i]);
            }
        }
    }
}

void ritardoComplessivo(corsa corse[MAX_CORSE_LENGTH], char codiceTratta[MAX_STRING_LENGTH + 1], int rows)
{
    int sum = 0;

    for (int i = 0; i < rows; i++) {
        if (corse[i].ritardo > 0) {
            // Normalizzo il codice tratta
            strToLower(corse[i].codice_tratta);

            if (strcmp(corse[i].codice_tratta, codiceTratta) == 0) {
                // Aggiungo alla somma dei ritardi il ritardo della corsa attuale.
                sum += corse[i].ritardo;
            }
        }
    }

    printf("Ritardo complessivo: %d\n", sum);
}

// Ritorna 1 se value è compresa tra inizio e fine, 0 altrimenti.
int between(data inizio, data fine, data value)
{
    return dateCompare(inizio, value) >= 0 && dateCompare(value, fine) >= 0;
}

/*
 * Confronta due date, ritornando:
 * -1 se a > b
 * 1 se b > a
 * 0 se a == b
 * N.B: avrei potuto utilizzare anche strcmp() poichè le due date sono nello stesso formato, ma avendo utilizzato
 * due structs, ho preferito creare una funziona ad-hoc: lo trovo più elegante.
 */
int dateCompare(data a, data b)
{
    if (a.year > b.year) {
        return -1;
    } else if (b.year > a.year) {
        return 1;
    } else {
        // Stesso anno
        if (a.month > b.month) {
            return -1;
        } else if (b.month > a.month) {
            return 1;
        } else {
            // Stesso anno && stesso mese
            if (a.day > b.day) {
                return -1;
            } else if (b.day > a.day) {
                return 1;
            }
        }
    }
    // Le date sono equivalenti
    return 0;
}

// Stampa la corsa formattata a dovere.
void printCorsa(corsa value)
{
    printf(
            "Codice: %s, Partenza: %s, Destinazione: %s, Data: %s, Ora partenza: %s, Ora arrivo: %s, Ritardo: %d;\n",
            value.codice_tratta,
            value.partenza,
            value.destinazione,
            value.data,
            value.ora_partenza,
            value.ora_arrivo,
            value.ritardo
    );
}

// Sostituisce ogni carattere della stringa con il suo corrispondente in lowercase.
void strToLower(char str [MAX_STRING_LENGTH + 1])
{
    for (int i = 0; i < strlen(str); i++) {
        str[i] = (char) tolower(str[i]);
    }
}