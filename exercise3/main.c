/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/14/2020
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_WORD_LENGTH 25
#define MAX_SEARCHES 20
#define MAX_ROW_LENGTH 200
#define MAX_OCCURRENCES 10

// Struct che rappresenta una occorrenza.
typedef struct {
    // Quale occorrenza?
    char word [MAX_WORD_LENGTH + 1];
    // Posizione della parola in cui è stata trovata l'occorrenza
    int index;
} occurrence;

// Struct che rappresenta una parola da cercare
typedef struct {
    // Quale parola?
    char word [MAX_WORD_LENGTH + 1];
    // Quante volte nel testo?
    int occurrencesNumber;
    // Quali sono le occorrenze?
    occurrence occurrences[MAX_OCCURRENCES];
} search;

void strTolower(char str[MAX_WORD_LENGTH + 1]);
int insensitiveSubstring(char needle[MAX_WORD_LENGTH + 1], char haystack[MAX_WORD_LENGTH + 1]);

int main() {
    FILE* sequencesFile;
    FILE* textFile;
    int sequencesNumber, counter = 0;
    search searches[MAX_SEARCHES];
    char row[MAX_ROW_LENGTH + 1];
    char tmpWord[MAX_WORD_LENGTH + 1];
    int index;
    int k;
    int global = 0;

    // Apro i file.
    sequencesFile = fopen("sequenze.txt", "r");
    textFile = fopen("testo.txt", "r");

    // Gestione degli errori legati ai file.
    if (sequencesFile == NULL || textFile == NULL) {
        printf("File error(s).");
        return -1;
    }

    // Leggo il numero di sequenze
    fscanf(sequencesFile, "%d", &sequencesNumber);

    // Leggo le sequenze e creo le rispettive struct nel vettore.
    while (!feof(sequencesFile) && counter < sequencesNumber) {
        fscanf(sequencesFile, "%s", searches[counter].word);
        searches[counter].occurrencesNumber = 0;
        counter++;
    }

    fclose(sequencesFile);

    while (!feof(textFile)) {
        // Leggo la riga
        fgets(row, MAX_ROW_LENGTH + 1, textFile);

        // Per ogni carattere
        for (int i = 0; i < strlen(row); i++) {
            // Se è un carattere alfanumerico effettuo i controlli
            if (isalnum(row[i])) {
                // Posizione della parola
                global ++;
                // Posizione del primo carattere della parola
                k = i;
                index = 0;

                while(k < strlen(row) && (!isspace(row[k]) && !ispunct(row[k]))) {
                    // Copio la parola trovata e incremento i contatori.
                    tmpWord[index] = row[k];
                    k++;
                    index++;
                }

                // Aggiungo il terminatore di stringa.
                tmpWord[index] = '\0';

                // Per ogni sequenza
                for (int l = 0; l < sequencesNumber; l++) {
                    // Controllo se la sequenza è contenuta nella parola, con un massimo di 10 occorrenze.
                    if (insensitiveSubstring(searches[l].word, tmpWord) && searches[l].occurrencesNumber < 10) {
                        // Copio la parola nella struct.
                        strcpy(searches[l].occurrences[searches[l].occurrencesNumber].word, tmpWord);
                        // Imposto nella struct la posizione della parola.
                        searches[l].occurrences[searches[l].occurrencesNumber].index = global;
                        // Incremento il contatore di occorrenze.
                        searches[l].occurrencesNumber++;
                    }
                }
                // Incremento l'indice di tante posizioni quanti sono stati i caratteri dell'ultima parola.
                i += strlen(tmpWord);
            }
        }
    }

    // Chiudo il file.
    fclose(textFile);

    // Stampo l'output formattato a dovere.
    for (int i = 0; i < sequencesNumber; i++) {
        if (searches[i].occurrencesNumber > 0) {
            printf("La sequenza `%s` è contenuta in\n", searches[i].word);
            for (int j = 0; j < searches[i].occurrencesNumber; j++) {
                printf("\t`%s` (parola in posizione %d nel testo)\n", searches[i].occurrences[j].word, searches[i].occurrences[j].index);
            }
            printf("\n");
        }
    }

    return 0;
}

// Trasforma una stringa nella sua corrispettiva versione in minuscolo (lowercase).
void strTolower(char str[MAX_WORD_LENGTH + 1])
{
    for (int i = 0; i < strlen(str); i++) {
        str[i] = (char) tolower(str[i]);
    }
}

// Verifica se needle è sottostringa di haystack in maniera case insensitive, senza però modificare le stringhe di partenza.
int insensitiveSubstring(char needle[MAX_WORD_LENGTH + 1], char haystack[MAX_WORD_LENGTH + 1])
{
    // Mi servo di due stringhe di supporto per evitare di modificare le stringhe di partenza
    // (ricordiamo infatti che le stringhe, in quanto vettori, sono passati servendosi di puntatori).
    char n[MAX_WORD_LENGTH + 1];
    char h[MAX_WORD_LENGTH + 1];
    strcpy(n, needle);
    strcpy(h, haystack);
    strTolower(n);
    strTolower(h);

    return strstr(h, n) != NULL;
}